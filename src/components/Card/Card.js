import React from "react";
import "./Card.css";
function Card() {
	return (
		<div class="card" data-tip="300x2?/cache321d6c">
			<div class="card-icons">
				<div class="card-icons-quality">HD</div>
			</div>
			<a href="/film/red-notice.300x2" title="Red Notice" class="card-poster">
				<img src="https://static.bunnycdn.ru/i/cache/images/b/b0/b02d5a16eba363248cf559efab5ba696.jpg-w180" />
			</a>
			{/* <span class="card-imdb">
				<i class="fa fa-star"></i> 6.40
			</span> */}
			<h3>
				<a class="card-title" title="Red Notice" href="/film/red-notice.300x2">
					Red Notice
				</a>
			</h3>
			<div class="card-meta">
				2021 <i class="card-meta-dot"></i> 118 min{" "}
				<i class="card-meta-type">Movie</i>
			</div>
		</div>
	);
}

export default Card;
