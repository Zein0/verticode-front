import React, { useState } from "react";
import { useMediaQuery } from "react-responsive";
import { Link } from "react-router-dom";
import "./Navbar.css";
import { FiSearch } from "react-icons/fi";
import { HiUserCircle } from "react-icons/hi";
function Navbar() {
	const isMobile = useMediaQuery({ query: `(min-width: 700px)` });
	const [search, setSearch] = useState("");
	return (
		<div className="Nav">
			<p className="Nav-title">VertiCode</p>
			<div className="Nav-menu">
				<Link className="Nav-menu-child" to="movies">
					Home
				</Link>

				<Link className="Nav-menu-child" to="movies">
					Genre
				</Link>

				<Link className="Nav-menu-child" to="movies">
					Top Rated
				</Link>

				<Link className="Nav-menu-child" to="movies">
					Movies
				</Link>

				<Link className="Nav-menu-child" to="movies">
					Tv Series
				</Link>
			</div>
			<div className="Nav-search-bar">
				<FiSearch className="Nav-search-bar-icon" />
				<input
					className="Nav-search-bar-input"
					type="text"
					placeholder="Enter your keywords..."
				></input>
			</div>
			<div className="Nav-user">
				<HiUserCircle className="Nav-user-icon" />
				<p className="Nav-user-text">Login/Register</p>
			</div>
		</div>
	);
}

export default Navbar;
