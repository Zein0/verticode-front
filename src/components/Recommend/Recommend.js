import React, { useState } from "react";
import Card from "../Card/Card";

function Recommend() {
	const [arr, setArr] = useState([1, 1, 1, 1, 1, 1, 1, 1, 1]);
	return (
		// <div>
		// 	<div className="rec">
		// 		<h2 className="rec-title">Recommended</h2>
		// 		<div className="rec-tabs">
		// 			<span className="rec-active" data-name="movies">
		// 				<i className="fa fa-play-circle"></i> Movies
		// 			</span>
		// 			<span data-name="shows">
		// 				<i class="fa fa-list"></i> TV Shows
		// 			</span>
		// 			<span data-name="trending">
		// 				<i class="fa fa-chart-line"></i> Trending
		// 			</span>
		// 		</div>
		// 		<div class="rec-clearfix"></div>
		// 	</div>
		//     </div>
		<div className="rec-content">
			<div className="rec-content-filmlist">
				{arr.length > 0 && arr.map((ar, key) => <Card></Card>)}
			</div>
		</div>
	);
}

export default Recommend;
