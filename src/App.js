import {
	BrowserRouter as Router,
	Routes,
	Route,
	Redirect,
} from "react-router-dom";
import Footer from "./components/Footer/Footer";
import Navbar from "./components/Navbar/Navbar";
import Home from "./pages/Home/Home";

const App = () => {
	return (
		<Router>
			{/* <Navbar /> */}
			<Home />
			<Routes>
				<Route path="/" exact element={Home} />
			</Routes>
			<Footer />
		</Router>
	);
};

export default App;
